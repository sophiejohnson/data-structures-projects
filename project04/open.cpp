// open.cpp: Open Addressing Map

#include "map.h"

#include <stdexcept>

// Methods --------------------------------------------------------------------
OpenMap::OpenMap(double loadfact, size_t size){
			load_factor = loadfact;
			table_size = size;
			items = 0;
        		resize(table_size); // used to initialize table
}

void            OpenMap::insert(const std::string &key, const std::string &value) {
			int bucket = locate(key); // find bucket where key/value should be inserted
			table[bucket].first = key;		
			table[bucket].second = value;	
			items ++;
			// resizes table if load factor is exceeded
			if (float(items)/float(table_size) > load_factor){
				resize(table_size * 2);
			}
}

const Entry     OpenMap::search(const std::string &key) {
			// checks bucket where key should be, returns bucket's entry
			int bucket = locate(key);
			if (table[bucket].first == key){
				return table[bucket];
			}
			else { return NONE; }
}

void            OpenMap::dump(std::ostream &os, DumpFlag flag) {
		if (table == nullptr){return;}
		for (size_t i = 0; i < table_size; i++){
			if (table[i] != NONE){
        		switch (flag) {
            			case DUMP_KEY: os << table[i].first << std::endl; break;
            			case DUMP_VALUE: os << table[i].second << std::endl; break;
            			case DUMP_KEY_VALUE: os << table[i].first << "\t" << table[i].second << std::endl; break;
            			case DUMP_VALUE_KEY: os << table[i].second << "\t" << table[i].first << std::endl; break;
        		}
			}
		}

}

size_t          OpenMap::locate(const std::string &key) {
			int bucket = hfunc(key) % table_size;
			size_t step = 0;
			// checks that bucket contains specified key or is empty
			while (table[bucket] != NONE && table[bucket].first != key && step < table_size){
				bucket = (bucket + 1) % table_size;
				step ++;
			}
			// resizes table if no empty bucket can be found
			if (step >= table_size){
				resize(table_size * 2);
				locate(key);
			}
			return bucket;
}

void            OpenMap::resize(const size_t new_size) {
			// create new table with new size
   			Entry * new_table = new Entry[new_size];
                        Entry * old_table = table;
                        table = new_table;
                        size_t old_size = table_size;
                        table_size = new_size;
			// insert (rehash) values of old table into new table
                        if (old_table != nullptr){
                                for(size_t i = 0; i < old_size; i++){
                                        if(old_table[i] != NONE){
                                                insert(old_table[i].first, old_table[i].second);
                                        }
                                }
                        }
                        delete [] old_table;
		

}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:




