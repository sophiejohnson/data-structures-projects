// chained.cpp: Separate Chaining Map

#include "map.h"

#include <stdexcept>

// Methods --------------------------------------------------------------------
                //default constructor
                ChainedMap::ChainedMap(double load_fact, size_t table_s){
                    load_factor = load_fact;
                    table_size = table_s;
                    items = 0;
                    resize(table_size);
                }


//insert a key value pair into the hash table
void            ChainedMap::insert(const std::string &key, const std::string &value) {
    size_t hash = StringHasher{}(key);
    int bucket = hash % table_size;
    std::map<std::string,std::string>::iterator it;
    it = table[bucket].find(key);
    //update value for a key
    if(it != table[bucket].end()){
        table[bucket][key] = value;
    }
    //add new key value pair to table
    else{
        //check table load factor for resizing
        if((float) items/table_size >= load_factor){
            resize(2*table_size);
        }
            bucket = hash % table_size;
            table[bucket][key] = value;
            items ++;
    }
}

//search for key in hash table
const Entry     ChainedMap::search(const std::string &key) {
    size_t hash = StringHasher{}(key);
    int bucket = hash % table_size;
    std::map<std::string,std::string>::iterator it;
    it = table[bucket].find(key);
    if(it != table[bucket].end()){
        return Entry(key, it->second);
    }
    else{
        return NONE;
    }
}

//dump key and value in specified format
void            ChainedMap::dump(std::ostream &os, DumpFlag flag) {
    for(size_t i = 0; i < table_size; i++){
        std::map<std::string,std::string>::iterator it;
        for(it = table[i].begin(); it != table[i].end(); it++){
            switch(flag){
                case DUMP_KEY:          os << it->first << std::endl; break;
                case DUMP_VALUE:        os << it->second << std::endl; break;
                case DUMP_KEY_VALUE:    os << it->first << "\t" << it->second << std::endl; break;
                case DUMP_VALUE_KEY:    os << it->second << "\t" << it->first << std::endl; break;
            }
        }
    }
}

//resize hash table
void            ChainedMap::resize(const size_t new_size) {
    size_t temp_size = table_size;
    table_size = new_size;

    std::map<std::string,std::string> *temp = table;
    table = new std::map<std::string,std::string>[table_size];
    if(items > 0){
        items = 0;
        for(size_t i = 0; i < temp_size; i++){
            std::map<std::string,std::string>::iterator it;
            for(it = temp[i].begin(); it != temp[i].end(); it++){
                insert(it->first,it->second);
            }
        }
        delete []temp;
    }
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
