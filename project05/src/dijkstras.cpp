// dijsktras.cpp
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

// Struct for each tile
struct Tile {
    Tile(int x, int y): x(x), y(y) { }
    int x;
    int y;
};

// Comparision function for tiles
struct Compare_Tile {
    bool operator()(const Tile a, const Tile b) const {
        return a.x < b.x || (a.x == b.x && a.y < b.y);
    }   
};

// Struct for each edge between tiles
struct Edge {
    Edge(int c, Tile n, Tile p): cost(c), name(n), prev(p) { }
    int cost;
    Tile name;
    Tile prev;
};

// Comparison function for edges
struct Compare_Edge {
    bool operator()(const Edge& src, const Edge &dest) {
        return src.cost > dest.cost;
    }
};

// Main execution
int main(int argc, char *argv[]) {
    // Map to hold tile names and costs
    map<char, int> tiles;
    
    int tiles_n = 0;
    cin >> tiles_n;

    // Read in and insert all tile names/costs into map
    for (int i = 0; i < tiles_n; i++) {
        char name; int cost;
        cin >> name >> cost;
        tiles.insert({name, cost});
    }
            
    // Create empty board with specified number of rows, columns
    int map_rows = 0, map_columns = 0;
    cin >> map_rows >> map_columns;	
    char board[map_rows][map_columns] = {0};
    
    // Fill board with tiles
    for (int i = 0; i < map_rows; i++) {
        for (int j = 0; j < map_columns; j++) {
            char board_tile;
            cin >> board_tile;
            board[i][j] = board_tile;
        }
    }

    // Read in starting and ending coordinates
    int source_x, source_y, target_x, target_y;
    cin >> source_x >> source_y >> target_x >> target_y;
    
    // Insert start/end coordinates into source/target tiles 
    Tile source = Tile(source_x, source_y);
    Tile target = Tile(target_x, target_y);

    // Create priority queue with edges
    priority_queue<Edge, vector<Edge>, Compare_Edge> frontier;
    map<Tile, Tile, Compare_Tile> marked;
    map<Tile, int, Compare_Tile> costs;

    frontier.push(Edge(0, source, source));

    while (!frontier.empty()) {

        // Pop minimum edge from top
        Edge curr = frontier.top();
        frontier.pop();
        Tile name = curr.name;
        Tile prev = curr.prev;

        // If cost hasn't been recorded, record costs
        if (costs.count(name) == 0) {
            costs[name] = curr.cost;
        }   
        // Otherwise update cost and change source
        else {
            if (curr.cost < costs[name]) {
                costs[name] = curr.cost;
                auto it = marked.find(name); 
                it->second = prev;
            }
        }

        // Find new cost to get to all neighboring edges
        int cost = tiles[board[name.x][name.y]] + curr.cost;

        // Add neighboring edges to frontier if tile isn't marked 
        if (marked.count(name) == 0) {
            int left = 0, right = 0, up = 0, down = 0;
            // Right edge
            right = name.y + 1;
            if (right < map_columns) {
                frontier.push(Edge(cost, Tile(name.x, right), name));
            }
            // Left edge
            left = name.y - 1;
            if (left >= 0) {
                frontier.push(Edge(cost, Tile(name.x, left), name));
            }
            // Below edge 
            down = name.x + 1;
            if (down < map_rows) {
                frontier.push(Edge(cost, Tile(down, name.y), name));
            }
            // Above edge
            up = name.x - 1;
            if (up >= 0) {
                frontier.push(Edge(cost, Tile(up, name.y), name));
            }
            // Add tile to marked 
            marked.insert(make_pair(name, prev));
       }
    }

    // Reconstruct path and find total cost
    int total_cost = 0;
    vector<Tile> path = {};
    while (source.x != target.x || source.y != target.y) {
        // Add tile to path and record cost
        path.push_back(target);
        auto it = marked.find(target);
        target.x = it->second.x;
        target.y = it->second.y;
        total_cost += tiles[board[target.x][target.y]];
    }
    // First tile
    path.push_back(target);

    // Display total cost and shorted path
    cout << total_cost << endl;
    auto it = path.end();
    while (it != path.begin()) {
        it--;
        cout << it->x << " " << it->y << endl;
    }

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
