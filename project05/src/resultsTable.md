Each trial run 5 times with a different randomly generated map

| N     | Elapsed Time  | Memory Usage      |
|-------|---------------|-------------------|
| 10    | 0.002998 s    | 1.183594 Mbytes   |
| 20    | 0.006998 s    | 1.226562 Mbytes   |
| 50    | 0.034593 s    | 1.488281 Mbytes   |
| 100   | 0.149576 s    | 2.417969 Mbytes   |
| 200   | 0.657899 s    | 6.132812 Mbytes   |
| 500   | 4.977642 s    | 32.00703 Mbytes   |
| 1000  | 22.19603 s    | 124.3672 Mbytes   | 
