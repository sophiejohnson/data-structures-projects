// merge.cpp

#include "lsort.h"

#include <iostream>
using namespace std;

// Prototypes
Node *msort(Node *head, CompareFunction compare);
void  split(Node *head, Node *&left, Node *&right);
Node *merge(Node *left, Node *right, CompareFunction compare);

// Implementations
void merge_sort(List &l, bool numeric) {
    // Numeric sort
    if (numeric) {
        l.head = msort(l.head, node_number_compare);
    }
    // String sort
    else {
        l.head = msort(l.head, node_string_compare);
    }
}

// Recursive function
Node *msort(Node *head, CompareFunction compare) {
    // Base case
    if (head->next == nullptr) {
        return head; 
    }
    // Divide into left and right sublists
    Node *left;
    Node *right;
    Node *l;
    Node *r;
    split(head, left, right);
    // Conquer left and right sublists
    l = msort(left, compare);
    r = msort(right, compare);
    // Combine
    head = merge(l, r, compare);
    // Return head
    return head;
}

// Split list in two
void split(Node *head, Node *&left, Node *&right) {
    // Traverse list to find center
    Node *fast = head;
    Node *slow = head;
    while (fast != nullptr) {
        fast = fast->next;
        if (fast != nullptr) {
            fast = fast->next;
        }
        if (fast != nullptr) {
            slow = slow->next;
        }
    }
    // Divide into two
    left = head;
    right = slow->next;
    slow->next = nullptr;
}

// Combine left and right lists
Node *merge(Node *left, Node *right, CompareFunction compare) {
    Node *newHead;
    Node *temp;
    
    // Determine head of new list
    bool isGreater = compare(left, right);
    if (isGreater) {
        newHead = left;
        left = left->next;
    }
    else {
        newHead = right;
        right = right->next;
    }
    temp = newHead;
    // Add next smallest element to list 
    while (left != nullptr && right != nullptr) {
        isGreater = compare(left, right);
        if (isGreater) {
            temp->next = left;
            temp = left;
            left = left->next;
        }
        else {
            temp->next = right;
            temp = right;
            right = right->next;
        }
    }
    // Add remaining elements of left or right side
    if (left != nullptr) {
        temp->next = left;
    }   
    else if (right != nullptr) {
        temp->next = right;
    }
    // Return head of sorted list
    return newHead;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
