// quick.cpp

#include "lsort.h"

#include <iostream>

using namespace std;

// Prototypes

Node *qsort(Node *head, CompareFunction compare);
void  partition(Node *head, Node *pivot, Node *&left, Node *&right, CompareFunction compare);
Node *concatenate(Node *left, Node *right);


// Implementations

void quick_sort(List &l, bool numeric) {
	if (numeric == 1){
		l.head = qsort(l.head, node_number_compare);
	}
	else{
		l.head = qsort(l.head, node_string_compare);
	}
}


Node *qsort(Node *head, CompareFunction compare) {
	// Handle base case
		if (head != nullptr){
			if (head->next  == nullptr){
			return head;
		}
	}

	// Divide into left and right sublists
	Node *left = nullptr;
	Node *right = nullptr;
	Node *leftSub;
	Node *rightSub;
	partition(head->next, head, left, right, compare);	

	// Conquer left and right sublists
	leftSub = qsort(left, compare);
	rightSub = qsort(right, compare);		

	// Combine left and right sublists
	head = concatenate(leftSub, rightSub);

	// Return head
	return head;
}

void partition(Node *head, Node *pivot, Node *&left, Node *&right, CompareFunction compare) {
	Node * curr = head;
	Node * temp;
	
	bool atLeastOne = false;

	while(curr != nullptr){
		bool smaller = compare(curr, pivot);
		temp = curr -> next;
		if (smaller){
			curr->next = left;
			left = curr;
			atLeastOne = true;
		}
		else{
			curr->next = right;
			right = curr;
		}
		curr = temp;
	}
	if (atLeastOne == true){
		pivot -> next = right;
		right = pivot;
	}
	else {
		pivot -> next = left;
		left = pivot;
	}

}


Node *concatenate(Node *left, Node *right) {
// return head of left
	Node * curr = left;
	if (left == nullptr){
		return right;

	}
	while(curr->next != nullptr){
		curr = curr -> next;
	}
	curr -> next = right;
	return left; 
}





// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
