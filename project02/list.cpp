// list.cpp

#include "lsort.h"
#include <memory>

#include <iostream>
using namespace std;

// Initialize empty list
List::List() {
    head = nullptr;
    size = 0;
}

// Delete all allocated memory from list
List::~List() {
    Node *next = nullptr;
    for (Node *curr = head; curr != nullptr; curr = next) {
        next = curr->next;
        delete curr;
    }
}

// Add new node to front of list with string, int, and pointer to next node
void List::push_front(const std::string &s) {
    int num = 0;
    size_t init = 0;
    size_t * ptr = &init;
    num = stoi(s, ptr, 10);
    head = new Node{s, num, head};
    size++;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
