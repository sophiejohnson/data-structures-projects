// node.cpp

#include "lsort.h"

#include <iostream>
using namespace std;

// C++ numeric comparison
bool node_number_compare(const Node *a, const Node *b) {
    return a->number < b->number;
}

// C++ string comparison
bool node_string_compare(const Node *a, const Node *b) {
    return a->string < b->string;
}

// C numeric comparison
int void_number_compare(const void *a, const void *b) {
    Node *na = *(Node **)a;
    Node *nb = *(Node **)b;
    return na->number > nb->number;
}

// C string comparison
int void_string_compare(const void *a, const void *b) {
    Node *na = *(Node **)a;
    Node *nb = *(Node **)b;
    return na->string.compare(nb->string);
}

// Display contents of list 
void dump_node(Node *n) {
    for (Node *curr = n; curr != nullptr; curr = curr->next){
        cout << "String: " << curr->string << "     ";
        cout << "Number: " << curr->number << endl;
    }
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
