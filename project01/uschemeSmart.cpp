// uschemeSmart.cpp

#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <unistd.h>
#include <memory>

using namespace std;

// Globals ---------------------------------------------------------------------

bool BATCH = false;
bool DEBUG = false;

// Structures ------------------------------------------------------------------
struct Node {
    Node(string value, shared_ptr<Node> left, shared_ptr<Node> right):value(value),left(left),right(right){} 
    string value;
    shared_ptr<Node> left;
    shared_ptr<Node> right;

    friend ostream &operator<<(ostream &os, const Node &n);
};

// Output contents of binary tree
ostream &operator<<(ostream &os, const Node &n) {
    if (!&n){
        os << "tree is empty" << endl;
    }
    cout << "(Node: value=" << n.value;
    if (n.left != nullptr){
        os << ", left=" << *(n.left);
    }
    if (n.right != nullptr){
        os  << ", right=" << *(n.right);
    }
    os << ")";
    return os;
}

// Parser ----------------------------------------------------------------------
#define Unique_Node(x,y,z) shared_ptr<Node>(new Node{x,y,z})

// Parse the next digit, parenthesis, or operation
string parse_token(istream &s) {
    string token;
    string x;
    x = s.peek();
    while(x == " "){
        s.get();
        x = s.peek();
    }
    if (x == "(" || x == ")" || x =="-" || x == "+" || x == "/" || x == "*"){
        token = s.get();
    }
    else if (isdigit(s.peek())){
        token.push_back(s.get());
        while(isdigit(s.peek())){
            token.push_back(s.get());
        }
    }
    return token;
}

// Store input in nodes to form binary tree
shared_ptr<Node> parse_expression(istream &s){
    string token;
    auto left = shared_ptr<Node>(nullptr);
    auto right = shared_ptr<Node>(nullptr);  
    token = parse_token(s);
    if (token == " " || token == ")"){
        return nullptr;
    }
    if (token == "("){
        token = parse_token(s);
        left = shared_ptr<Node>(parse_expression(s));
    	if (left){
	    right = shared_ptr<Node>(parse_expression(s));
        }
        if (right){
            parse_token(s);
        }
    } 
    return Unique_Node(token, left, right); 
}

// Interpreter -----------------------------------------------------------------

// Recursive function to evaluate scheme expression
void evaluate_r(const shared_ptr<Node> &n, stack<int> &s) {
    // Post order recursion
    if(n == nullptr) return;
    if(n->right) evaluate_r(n->right, s);
    if(n->left) evaluate_r(n->left, s);
    
    // Perform appropriate operation with two numbers on top of stack
    string x = n->value;
    if(x == "+" || x == "-" || x == "*" || x == "/"){
        int y1 = s.top();
        s.pop();
        int y2 = s.top();
        s.pop();
        switch(x[0]){            
            case '+':
                s.push(y1 + y2);
                break;
            case '-':
                s.push(y1 - y2);
                break;
            case '*':
                s.push(y1 * y2);
                break;
            case '/':
                s.push(y1 / y2);
        }
    }
    // If numeric, convert to int and add to stack
    else{
        int z;
        stringstream ss;
        ss << n->value;
        ss >> z;
        s.push(z);
   }
}

// Call to recursive function to evaluate
int evaluate(const shared_ptr<Node> &n) {
    stack<int> s;
    evaluate_r(n, s);
    return s.top();
}

// Main execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    string line;
    int c;

    while ((c = getopt(argc, argv, "bdh")) != -1) {
        switch (c) {
            case 'b': BATCH = true; break;
            case 'd': DEBUG = true; break;
            default:
                cerr << "usage: " << argv[0] << endl;
                cerr << "    -b Batch mode (disable prompt)"   << endl;
                cerr << "    -d Debug mode (display messages)" << endl;
                return 1;
        }
    }

    while (!cin.eof()) {
        // Display prompt
        if (!BATCH) {
            cout << ">>> ";
            cout.flush();
        }
        // End program if no more input
        if (!getline(cin, line)) {
            break;
        }
        if (DEBUG) { cout << "LINE: " << line << endl; }
        
        // Parse expression
        stringstream s(line);
        auto n = shared_ptr<Node>(parse_expression(s));
        if (DEBUG) { cout << "TREE: " << *n << endl; }

        // Evaluate
        cout << evaluate(n) << endl;

    }

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
