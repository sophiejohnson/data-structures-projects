Project 01: Arithmetic Scheme Interpreter
=========================================

Overview
--------
This is a simple arithmetic Scheme interpreter in C++.

Method
------
- Read in a line of Scheme code from standard input, parse the text, and construct an abstract syntax tree
- Evaluate the abstract syntax tree by traversing it and simplifying expressions into a single value
- Display the result of the interpretation
