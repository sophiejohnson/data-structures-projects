fiND my run
===========
- Developed a web application using Python in Jupyter Notebook with Appmode extension
- Implemented a graph traversal algorithm with backtracking to generate potential running routes based on a selected map, specified starting point, target distance, and maximum distance
- Used Python library Matplotlib to display a running route in each direction from the initial position


Project Dependencies
====================
#### NOTE: This README must be viewed in the GitLab repository for the images to show up

The project is set up in Jupyter notebooks, so there are several dependencies for the project to work. It uses an extension called appmode, so this cannot be run on the student machines. On your local machine, download and install Anaconda 3. After installation, open up the Anaconda prompt. Run the following command to install the appmode extension:

![Anaconda Prompt](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/prompt.PNG?raw=true)
 
Once installation finishes, the app can be run. 


Running the User Interface
==========================
Open Anaconda Navigator and click on Jupyter Notebook.

![Anaconda Navigator](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/anaconda.PNG?raw=true)

Navigate to wherever "findMyRun.ipynb" is stored and open it.

![Opening Jupyter Notebook](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/findMyRun.PNG?raw=true)


To open appmode to use the user interface, click on the appmode button.

![Appmode Button](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/appmode%20button.PNG?raw=true)

Follow the usage instructions denoted in the app.

![App Usage Instructions](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/usage.PNG?raw=true)

Running Other Jupyter Notebooks
===============================
Other Jupyter notebooks can be run by starting at the top of the page and clicking run cell through all of the cells to load the necessary functions and values to run the program. Then then main function can be run by pressing the same button. 

![Play Button](https://gitlab.com/kmacdone/projects/raw/master/finalProject/mdPics/play%20button.PNG?raw=true)
 
This would be how you run the test function.

Group Member Contributions
==========================

1. Danielle Galvao   (dgalvao)   - graph generation, benchmarking, video
2. Sophie Johnson    (sjohns37)  - weighted adjacency list representation, path finding algorithm 
3. Keith MacDonell   (kmacdone)  - visualization functions, test function, web application, README.md