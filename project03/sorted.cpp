// sorted.cpp: Sorted Map

#include "map.h"

#include <algorithm>

// Type Definitions ------------------------------------------------------------

typedef std::vector<Entry>::iterator IT;

// Prototypes ------------------------------------------------------------------

const Entry     binary_search(const IT &start, const IT &end, const std::string &key);

// Methods ---------------------------------------------------------------------

// Insert entry in sorted list
void            SortedMap::insert(const std::string &key, const std::string &value) {
    // Add to beginning if empty
    if (entries.empty()) {
        entries.push_back(Entry{key, value});
    }
    else {
        // Traverse list to find proper place
        IT it = entries.begin();
        while(it != entries.end() && (*it).first < key) {
            it++;
        }
        // Add entry (add to end, update value, or insert)
        if (it == entries.end()) {
            entries.push_back(Entry{key, value});
        }
        else if ((*it).first == key) {
            (*it).second = value;
        }
        else {
            entries.insert(it, Entry{key, value});
        }
    }
}

// Search function
const Entry     SortedMap::search(const std::string &key) {
    Entry found = NONE;
    if (!entries.empty()) {
        found = binary_search(entries.begin(), entries.end() - 1, key);
    }
    return found;
}

// Dump contents of sorted map
void            SortedMap::dump(std::ostream &os, DumpFlag flag) {
   switch (flag) {
        case DUMP_KEY:
            for (IT it = entries.begin(); it != entries.end(); it++) {
                os << (*it).first << std::endl;
            }
            break;
        case DUMP_VALUE:
            for (IT it = entries.begin(); it != entries.end(); it++) {
                os << (*it).second << std::endl;
            }
            break;
        case DUMP_KEY_VALUE:
            for (IT it = entries.begin(); it != entries.end(); it++) {
                os << (*it).first << '\t' << (*it).second << std::endl;
            }
            break;
        case DUMP_VALUE_KEY:
             for (IT it = entries.begin(); it != entries.end(); it++) {
                os << (*it).second << '\t' << (*it).first << std::endl;
            }
            break;
    }              
}

// Internal Functions ----------------------------------------------------------

// Recursive binary search function
const Entry   binary_search(const IT &start, const IT &end, const std::string &target) {
    auto middle = (end - start) / 2;
    auto midpoint = *(start + middle);
    // Base case for matching entry
    if (target.compare(midpoint.first) == 0) {
        return midpoint;
    }
    // Base case for no matching entry
    if (start == end) {
        return NONE;
    }// Recursive calls on left or right side of vector
    if (target.compare(midpoint.first) < 0) {
        return binary_search(start, start + middle, target);
    }
    else {
        return binary_search(start + middle + 1, end, target);
    }
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
