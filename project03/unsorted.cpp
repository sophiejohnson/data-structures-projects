// unsorted.cpp: Unsorted Map

#include "map.h"

// Methods --------------------------------------------------------------------

// Insert key/value pair
void            UnsortedMap::insert(const std::string &key, const std::string &value) {
    bool inserted = false;
    for (auto it = entries.begin(); it != entries.end(); it++) {
        if ((*it).first == key) {
            (*it).second = value;
            inserted = true;
        }
    }
    if (!inserted) {
        entries.push_back(Entry{key, value});
    }
}

// Search for key in map
const Entry     UnsortedMap::search(const std::string &key) {
    Entry found = NONE;
    for (auto it = entries.begin(); it != entries.end(); it++) {
        if ((*it).first == key) {
            found = *it;
        }
    }
    return found;
}

// Dump contents of unsorted map
void            UnsortedMap::dump(std::ostream &os, DumpFlag flag) {
    switch (flag) {
        case DUMP_KEY:
            for (auto it = entries.begin(); it != entries.end(); it++) {
                os << (*it).first << std::endl;
            }
            break;
        case DUMP_VALUE:
            for (auto it = entries.begin(); it != entries.end(); it++) {
                os << (*it).second << std::endl;
            }
            break;
        case DUMP_KEY_VALUE:
            for (auto it = entries.begin(); it != entries.end(); it++) {
                os << (*it).first << '\t' << (*it).second << std::endl;
            }
            break;

        case DUMP_VALUE_KEY:
            for (auto it = entries.begin(); it != entries.end(); it++) {
                os << (*it).second << '\t' << (*it).first << std::endl;
            }
    }
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
