| Backend     | Text                    | File Size | Elasped Time  |
|-------------|-------------------------|-----------|---------------|
| UNSORTED    | pride_and_prejudice.txt | 724725    | 2m14.179s     |
| SORTED      |                         |           | 0m44.600s     |
| BST         |                         |           | 0m0.409s      |
| RBTREE      |                         |           | 0m0.436s      |
| TREAP       |                         |           | 0m0.567s      |
| UNSORTED    | bible.txt               | 4452070   | 38m20.762s    |
| SORTED      |                         |           | 15m46.766s    |
| BST         |                         |           | 0m2.549s      |
| RBTREE      |                         |           | 0m2.859s      |
| TREAP       |                         |           | 0m3.144s      |
| UNSORTED    | jungle_book.txt         | 304376    | 0m35.353s     |
| SORTED      |                         |           | 0m11.400s     |
| BST         |                         |           | 0m0.202s      |
| RBTREE      |                         |           | 0m0.210s      |
| TREAP       |                         |           | 0m0.281s      |

1.
Unsorted:
The unsorted backend has O(n) best, average, and worst case complexities for insertion. Each time a value is inserted, every element must be checked to see if the value is already in the vector. For searching, the unsorted backend has O(1) best case complexity, which occurs when the value is the first element in the list. Otherwise, it has O(n) average and worst case complexities because every element must be checked to see if a value is in the vector.

Sorted:
The sorted backend has O(1) best case complexity, which occurs when the element's proper position is the beginning of the vector. Average and worst case complexities for insertion are O(n) because the entire vector is traversed to find the proper place for the new value. Searching with the sorted backend has O(1),which occurs when the binary search function checks the value first. Otherwise, the binary search ensures O(log n) average and worst case complexities for searching the vector.

BST:
The binary search tree backend has O(1) best case for insertion, which occurs when it updates the value of the root. It has O(log n) average case complexity because a fairly balanced binary search tree will traverse its depth of log n in order to insert the value. It has O(n) worst case complexity because a binary tree could have a depth of n which would need to be traversed. For searching, the binary search tree backend has O(1) best case complexity, which occurs when the root contains the value. It has O(log n) average case complexity and O(n) worst case complexity because the depth (on average log n, in worst cases n) must be traversed to search the elements.

RBTree:
The red black tree backend has O(1) best case for insertion, which occurs when it updates the value of the root. It has O(log n) average and worst case complexity because a red black tree always maintains a depth of log n, which must be traversed to find the proper place to insert the value. For searching, the red black tree has O(1) best case complexity when the root contains the value. Otherwise, its depth of log n ensures an average and worst case complexity of O(log n).

Treap:
The treap backend has O(1) best case complexity for insertion when the inserted value updates the root. It has O(log n) average case complexity for insertion because a treap usually has a depth of log n to be traversed upon insertion and O(n) worst case complexity for cases in which the depth is n. For searching, the treap has bast case complexity of O(1) when the value is the root. It has O(log n) average case and O(n) worst case complexities for searching because the height of the treap (on average log n, in worst cases n) must be traversed to find the value.
