// bst.cpp: BST Map

#include "map.h"

#include <stdexcept>

// Prototypes ------------------------------------------------------------------

Node *insert_r(Node *node, const std::string &key, const std::string &value);
Node *search_r(Node *node, const std::string &key);
void    dump_r(Node *node, std::ostream &os, DumpFlag flag);

// Methods ---------------------------------------------------------------------

void            BSTMap::insert(const std::string &key, const std::string &value) {
			root = insert_r(root, key, value);
}

const Entry     BSTMap::search(const std::string &key) {
			auto result = search_r(root,key);
        		if (result == nullptr){
                		return NONE;
			}
        		else{
                		return result-> entry;
			}
}

void            BSTMap::dump(std::ostream &os, DumpFlag flag) {
			dump_r(root, os, flag);
}

// Internal Functions ----------------------------------------------------------

Node *insert_r(Node *node, const std::string &key, const std::string &value) {
	// base case
	if(node == nullptr){
		node = new Node;
                node -> entry.first = key;
                node -> entry.second = value;
                node -> priority = 0;
               	node -> left = nullptr;
                node -> right = nullptr;
	}
        // if value less than or equal to value of root, compare with left child and recurse
        else if (key <= node -> entry.first){
		// if key is equal to node's key, replace node's value with new value
		if(key == node ->entry.first){
			node-> entry.second = value;
		}
		else{
                        node -> left = insert_r(node -> left, key, value);
		}
	}
       	// if value greater than value of root, compare with right child and recurse
        else if (key > node -> entry.first){
                node -> right = insert_r(node -> right, key, value);
       	}
       	return node;
}


Node *search_r(Node *node, const std::string &key) {
	// base case
	if(node == nullptr || key == node->entry.first){
		return node;
    	}
	// if key is smaller than root's key, recurse through left subtree
    	else if(key < node->entry.first){
        	return search_r(node->left, key);
    	}
	// if key is larger than root's key, recurse through right subtree
    	else{ 
		return search_r(node->right, key); 
	}
}	


void dump_r(Node *node, std::ostream &os, DumpFlag flag) {
	// base case
	if (node == nullptr){return;}

     	// recurse through left subtree
     	dump_r(node->left, os, flag);

     	// print the data of node
        switch (flag) {
            case DUMP_KEY:          os << node->entry.first  << std::endl; break;
            case DUMP_VALUE:        os << node->entry.second << std::endl; break;
            case DUMP_KEY_VALUE:    os << node->entry.first  << "\t" << node->entry.second << std::endl; break;
            case DUMP_VALUE_KEY:    os << node->entry.second << "\t" << node->entry.first  << std::endl; break;
        }
        // recurse through right subtree 
     	dump_r(node->right, os, flag);
}


// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
