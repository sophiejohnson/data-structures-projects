// treap.cpp: Treap Map

#include "map.h"

#include <climits>
#include <random>

// Prototypes ------------------------------------------------------------------

static Node *insert_r(Node *node, const std::string &key, const std::string &value);
extern Node *search_r(Node *node, const std::string &key);
extern void    dump_r(Node *node, std::ostream &os, DumpFlag flag);
static Node *rotate_right(Node *p);
static Node *rotate_left(Node *p);
static int get_random();
void deleteTreap(Node *node);

// Methods ---------------------------------------------------------------------

void            TreapMap::insert(const std::string &key, const std::string &value) {
    root = insert_r(root, key, value); 
}

const Entry     TreapMap::search(const std::string &key) {
    auto result = search_r(root,key);
    if (result == nullptr){
        return NONE;
    } 
    else{
        return result-> entry;
    }
}

void            TreapMap::dump(std::ostream &os, DumpFlag flag) {
    dump_r(root, os, flag);
}
                TreapMap::~TreapMap(){ deleteTreap(root); }

// Internal Functions ----------------------------------------------------------
// helper function to delete treap
void deleteTreap(Node *node){
    if(node != nullptr){
        deleteTreap(node->left);
        deleteTreap(node->right);
        delete node;
    }
}

//insert key, value pair into treap
Node *insert_r(Node *node, const std::string &key, const std::string &value) {
    if(node == nullptr){    //add new key, value pair to treap
        node = new Node;
        node -> entry.first = key;
        node -> entry.second = value;
        node-> priority = get_random();
        node -> left = nullptr;
        node -> right = nullptr;
        return node;
    }
    else if(node->entry.first == key){  //update value for a key in treap
        node->entry.second = value;
        return node;
    }
    else{   //recursive call and necessary rotations by priority
        if(key < node->entry.first){
            node->left = insert_r(node->left, key, value);
            if(node->priority < node->left->priority){
                node = rotate_right(node);
            }
        }
        else{
            node->right = insert_r(node->right, key, value);
            if(node->priority < node->right->priority){
                node = rotate_left(node);
            }
        }
    }
    return node;
}

//right rotation 
Node *rotate_right(Node *p) {
    Node *newParent = p->left;
    Node *newChild = newParent->right;
    newParent->right = p;
    p->left = newChild;
    return newParent;
}

//left rotation
Node *rotate_left(Node *p) {
    Node *newParent = p->right;
    Node *newChild = newParent->left;
    newParent->left = p;
    p->right = newChild;
    return newParent;
}

//generate a random number for the node's priority
int get_random() {
    std::random_device rd;
    std::default_random_engine g(rd());
    std::uniform_int_distribution<int> d(1, INT_MAX);
    return d(g);
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
